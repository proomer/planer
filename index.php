<?php

include('bootstrap.php');

use Planer\Geometry\Polygon2D;
use Planer\Geometry\Vector2D;
use Planer\Plan\Plans;
use Planer\Plan\Plan;
use Planer\Image\RasterImageGenerator;
use Planer\Image\SVGImageGenerator;
use Tracy\Debugger;
use Szenis\Router;
use Szenis\RouteResolver;

error_reporting(E_ALL);
ini_set('display_errors', 1);

Debugger::enable();

$router = new Router();

$router->add('/', 'GET', function() {
    include 'template\index.html';
});

$router->add('/rest', 'GET', function() {
    
});

$router->add('/list', 'GET', function() {
    $plansManager = new Plans;
    $plans = $plansManager->findAll();
    /*
      $imageDriver = new RasterImageGenerator(300, 300);
      $imageType = RasterImageGenerator::FILE_TYPE_PNG;
     */

    $imageDriver = new SVGImageGenerator(300, 300);
    $imageType = SVGImageGenerator::FILE_TYPE_SVG;

    foreach ($plans as $plan)
    {
        $img = $plan->saveImage($imageDriver, $imageType);
        //echo "<img src=\"$img\"><br/>";
        echo "<object data=\"$img\"></object>";
    }
});

$resolver = new RouteResolver($router);

try
{
    // You have to resolve the route inside the try block
    $resolver->resolve([
        'uri' => $_SERVER['REQUEST_URI'],
        'method' => $_SERVER['REQUEST_METHOD'],
    ]);
}
catch (Szenis\Exceptions\RouteNotFoundException $e)
{
    die($e->getMessage());
}
catch (Szenis\Exceptions\InvalidArgumentException $e)
{
    die($e->getMessage());
}

/*
  $geometry = geoPHP::load($polygon->getGeometry(), 'json');
  var_dump($geometry->getArea());
  print( "This multipoint has " . $geometry->numGeometries() . " points.");
 */


//$result = $collection->insertOne( [ 'name' => 'Hinterland', 'brewery' => 'BrewDog' ] );
//echo "Inserted with Object ID '{$result->getInsertedId()}'";
