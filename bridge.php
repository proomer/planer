<?php

include('bootstrap.php');

require 'vendor/autoload.php';

if (isset($_POST['com']) && $_POST['com'] == 'save')
{
    $answer = array(
        'code' => 200,
        'com' => $_POST['com'],
        'data' => json_decode($_POST['data'], TRUE),
    );

    $plans = new Plans();
    $plan = $plans->searchByGeom($answer['data']);
    $answer['plan'] = $plan;


    $polygon = new Polygon2D();
    $points = Plans::relocatePoints($answer['data']);
    foreach ($points as $point)
    {
        $polygon->addPoint(new Vector2D($point['x'], $point['y']));
    }
    $polygon->testDraw(true);
}

echo json_encode($answer);


