<?php

namespace Planer\Image;

/**
 *
 * @author IvanSCM
 */
interface ImageGenerator {

    public function __construct($width, $height);

    public function save($type, $filename);

    public function draw();

    public function setGeometry($geometry);

    public function setLineColor($color);

    public function setFillColor($color);

    public function setLineThickness($thickness);
}
