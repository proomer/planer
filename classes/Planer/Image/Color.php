<?php

namespace Planer\Image;

/**
 * Модуль помогает работать с цветами для изображений.
 * @author IvanSCM <ivanscm@gmail.com>
 * @package Planer\Image
 * @subpackage Planer\Image\Color
 */
final class Color {

    /**
     * Черный цвет
     */
    static public $colorBlack = array(0, 0, 0);

    /**
     * Белый цвет
     */
    static public $colorWhite = array(255, 255, 255);

    /**
     * Серый цвет
     */
    static public $colorGrey = array(128, 128, 128);

    /**
     * Преобразует массив с цветом
     * @param array $arColor Red, Green, Blue
     * @return string format: rgb( [0-255], [0-255], [0-255])
     */
    public static function convertColorToStrRGB($arColor)
    {
        $strColor = implode(', ', $arColor);
        return "rgb({$strColor})";
    }

    /**
     * Преобразует массив с цветом в GD цвет
     * @param array $arColor Red, Green, Blue
     * @param resource $image Ресурс изображения GD
     * @return int
     */
    public static function convertColorToGD($arColor, $image)
    {
        return imagecolorallocate($image, $arColor[0], $arColor[1], $arColor[2]);
    }

}
