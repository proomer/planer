<?php

namespace Planer\Image;

use Planer\Image\Color;
use Planer\Image\ImageGeneratorException;

/**
 * Модуль работы с SVG изображениями.
 * @author IvanSCM <ivanscm@gmail.com>
 * @package Planer\Image
 */
final class SVGImageGenerator implements ImageGenerator {

    const FILE_TYPE_SVG = 'svg';

    private $fileName;

    /**
     *
     * @var string
     */
    private $image;

    /**
     * Массив точек
     * @var array of Vector2D
     */
    private $points;

    /**
     * Ширина изображения
     * @var integer пиксел
     */
    private $width;

    /**
     * Высота изображения
     * @var integer писксел 
     */
    private $height;

    /**
     * Толщина линии
     * @var integer  
     */
    private $thickness;

    /**
     * Цвет линии
     * @var string format: rgb( [0-255], [0-255], [0-255])  
     */
    private $strokeСolor;

    /**
     * Цвет заливки
     * @var string format: rgb( [0-255], [0-255], [0-255])  
     */
    private $fillColor;

    /**
     * Цвет фона
     * @var string format: rgb( [0-255], [0-255], [0-255]) 
     */
    private $backgroundColor;

    public function __construct($width, $height)
    {
        $this->width = $width;
        $this->height = $height;

        // set default values
        $this->strokeСolor = Color::convertColorToStrRGB(Color::$colorBlack);
        $this->fillColor = Color::convertColorToStrRGB(Color::$colorGrey);
        $this->backgroundColor = Color::convertColorToStrRGB(Color::$colorWhite);
        $this->thickness = 1;
    }

    public function save($type, $filename)
    {
        $this->fileName = $filename;
        $this->draw();
        if (!file_put_contents($filename, $this->image))
        {
            throw new ImageGeneratorException('Невозможно сохранить изображение');
        }
        return $this->fileName;
    }

    public function setGeometry($geometry)
    {
        $this->points = $geometry->points;
    }

    public function setFillColor($color)
    {
        $this->fillColor = $color;
    }

    public function setLineThickness($thickness)
    {
        $this->thickness = $thickness;
    }

    public function setLineColor($color)
    {
        $this->color = $color;
    }

    public function draw()
    {
        $svg = new \SimpleXMLElement('<svg></svg>');
        $svg->addAttribute('version', '1.1');
        $svg->addAttribute('baseProfile', 'full');
        $svg->addAttribute('xmlns', 'http://www.w3.org/2000/svg');
        $svg->addAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
        $svg->addAttribute('xmlns:ev', 'http://www.w3.org/2001/xml-events');
        $svg->addAttribute('height', "{$this->height}px");
        $svg->addAttribute('width', "{$this->width}px");
        $polyline = $svg->addChild('polyline');
        $polyline->addAttribute('fill', $this->fillColor);
        $polyline->addAttribute('stroke-width', $this->thickness);
        $polyline->addAttribute('stroke', $this->strokeСolor);

        $pointsStr = '';
        foreach ($this->points as $point)
        {
            $pointsStr .= "{$point} ";
        }
        $polyline->addAttribute('points', $pointsStr);
        $this->image = $svg->asXML();
    }

}
