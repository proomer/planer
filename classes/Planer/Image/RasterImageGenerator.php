<?php

namespace Planer\Image;

use Planer\Image\ImageGeneratorException;

/**
 * Модуль работы с растровыми изображениями.
 * @author IvanSCM <ivanscm@gmail.com>
 * @package Planer\Image
 */
final class RasterImageGenerator implements ImageGenerator {

    const FILE_TYPE_JPEG = 'jpeg',
            FILE_TYPE_PNG = 'png';

    private $fileName;

    /**
     * Ресурс GD изображения
     * @var resource 
     */
    private $image;

    /**
     * Массив точек
     * @var array of Vector2D
     */
    private $points;

    /**
     * Ширина изображения
     * @var integer пиксел
     */
    private $width;

    /**
     * Высота изображения
     * @var integer писксел 
     */
    private $height;

    /**
     * Толщина линии
     * @var integer  
     */
    private $thickness;

    /**
     * Цвет линии
     * @var int  
     */
    private $strokeСolor;

    /**
     * Цвет заливки
     * @var int  
     */
    private $fillColor;

    /**
     * Цвет фона
     * @var int
     */
    private $backgroundColor;

    public function __construct($width, $height)
    {
        // set default values
        $this->width = $width;
        $this->height = $height;

        $this->image = imagecreatetruecolor($this->width, $this->height);

        $this->strokeСolor = Color::convertColorToGD(Color::$colorBlack, $this->image);
        $this->fillColor = Color::convertColorToGD(Color::$colorGrey, $this->image);
        $this->backgroundColor = Color::convertColorToGD(Color::$colorWhite, $this->image);
        $this->thickness = 1;
    }

    public function save($type, $filename)
    {
        $this->fileName = $filename;
        $this->draw();
        switch ($type)
        {
            case self::FILE_TYPE_JPEG :
                $save = imagejpeg($this->image, $this->fileName, 100);
                break;
            case self::FILE_TYPE_PNG:
                $save = imagepng($this->image, $this->fileName, 0);
                break;
            default:
                $save = imagejpeg($this->image, $this->fileName, 100);
                break;
        }

        if (!$save)
        {
            throw new ImageGeneratorException('Невозможно сохранить изображение');
        }

        return $this->fileName;
    }

    public function setGeometry($geometry)
    {
        $this->points = $geometry->points;
    }

    public function setFillColor($color)
    {
        $this->fillColor = $color;
    }

    public function setLineThickness($thickness)
    {
        $this->thickness = $thickness;
    }

    public function setLineColor($color)
    {
        $this->color = $color;
    }

    public function draw()
    {
        imagefilledrectangle($this->image, 0, 0, $this->height, $this->height, $this->backgroundColor);

        $values = array();

        /** @var \Planer\Geometry\Vector2D $point */
        foreach ($this->points as $point)
        {
            $arPoint = $point->toArray();
            array_push($values, $arPoint['x'], $arPoint['y']);
        }

        imagefilledpolygon($this->image, $values, count($this->points), $this->fillColor);
    }

}
