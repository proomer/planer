<?php

namespace Planer\Geometry;

final class Polygon2D {

    public $points;

    public function __construct($points = array())
    {
        $this->points = $points;
    }

    public function addPoint(Vector2D $point)
    {
        $this->points[] = $point;
    }

    /**
     * Установить геометрию из базы данных
     * @param \Planer\Geometry\MongoDB\Model\BSONDocument $geometry Геометрия
     */
    public function setGeometry(\MongoDB\Model\BSONDocument $geometry)
    {
        foreach ($geometry->coordinates[0] as $coord)
        {
            $point = new Vector2D($coord[0], $coord[1]);
            $this->addPoint($point);
        }
    }

    public function getGeometry()
    {
        $geometry = array(
            'type' => 'Polygon',
            'coordinates' => array(
                0 => array()
            )
        );

        foreach ($this->points as $point)
        {
            $geometry['coordinates'][0][] = $point->toArray2();
        }

        return json_encode($geometry);
    }

}
