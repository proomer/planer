<?php

namespace Planer\Geometry;

final class Vector2D
{

    public $x;
    public $y;

    public function __construct($x = 0, $y = 0) {
        $this->x = $x;
        $this->y = $y;
    }

    public function toArray() {
        return array(
            'x' => $this->x,
            'y' => $this->y,
        );
    }

    public function toArray2() {
        return array(
            $this->x,
            $this->y,
        );
    }

    public function fromArray($ar) {
        if (isset($ar['x']))
            throw new Vector2DException("Coord X not found.");

        if (isset($ar['y']))
            throw new Vector2DException("Coord Y not found.");

        $this->x = $ar['x'];
        $this->y - $ar['y'];
    }

    public function __toString() {
        return "{$this->x}, {$this->y}";
    }

}

class Vector2DException extends \Exception
{
    
}
