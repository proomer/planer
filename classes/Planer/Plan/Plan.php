<?php

namespace Planer\Plan;

use Planer\Geometry\Vector2D;
use Planer\Geometry\Polygon2D;
use Planer\Image;

final class Plan {

    public $id;

    /**
     *
     * @var string Человекопонятное название
     */
    public $name;

    /**
     * 
     * @var Polygon2D Геометрия 
     */
    public $geometry;

    /**
     *
     * @var MongoDB\Model\BSONDocument Данные планировки с базы 
     */
    private $db_data;

    public function __construct(\MongoDB\Model\BSONDocument $db_data)
    {
        $this->db_data = $db_data;

        $this->id = (string) $this->db_data->_id;
        $this->name = $this->db_data->name;

        /* Построим геометрию */
        $this->geometry = new Polygon2D();
        $this->geometry->setGeometry($this->db_data->geom);
    }

    /**
     * Сохранить изображение планировки
     * @param \Planer\Image\ImageGenerator $driver Драйвер отрисовщика изображений
     * @return string Имя файла
     */
    public function saveImage(Image\ImageGenerator $driver, $type = NULL)
    {
        $fileName = 'img/' . uniqid($this->id . '_') . '.' . $type;
        $driver->setGeometry($this->geometry);
        return $driver->save($type, $fileName);
    }

    public function __toString()
    {
        return $this->id;
    }

}
