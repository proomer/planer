<?php

namespace Planer\Plan;

use \MongoDB\client;

final class Plans
{

    private $mongoClient;

    public function __construct() {
        $this->mongoClient = new Client("mongodb://localhost:27017");
    }

    private static function clearAr(&$item, $key) {
        if (is_array($item))
        {
            $item = array_values($item);
        }
    }

    public static function relocatePoints($points) {
        $x_min_index = 0;
        $y_min_index = 0;

        // search min coordinates

        for ($i_y = 1; $i_y <= count($points) - 1; $i_y++)
        {
            if ($points[$y_min_index]['y'] > $points[$i_y]['y'])
            {
                $y_min_index = $i_y;
            }
        }

        for ($i_x = 1; $i_x <= count($points) - 1; $i_x++)
        {
            if ($points[$x_min_index]['x'] > $points[$i_x]['x'])
            {
                $x_min_index = $i_x;
            }
        }

        $x_min_val = $points[$x_min_index]['x'];
        $y_min_val = $points[$y_min_index]['y'];

        // modify
        foreach ($points as &$item)
        {
            $item = array(
                'x' => $item['x'] - $x_min_val,
                'y' => $item['y'] - $y_min_val
            );
        }

        return $points;
    }

    public function searchByGeom($geom) {

        self::relocatePoints($geom);

        //array_walk($geom, 'self::clearAr');
        $coll = $this->mongoClient->local->plans->findOne(array(
            'geom' => array(
                'coordinates' => array(
                    0 => $geom
                ),
            ),
        ));
        return $coll;
    }

    public function add(Plan $plan) {
        $coll = $this->mongoClient->local->plans;
        $coll->insertOne();
    }

    /**
     * Search all plans
     * 
     * @return array Массив с планировками
     */
    public function findAll() {
        $plans = array();


        $collection = $this->mongoClient->local->plans;

        foreach ($collection->find() as $plan)
        {
            $plans[] = new Plan($plan);
        }

        return $plans;
    }

}
