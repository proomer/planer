<?php

// composer autoloader
require 'vendor/autoload.php';

function planer_autoloader($className) {
    $className = str_replace('\\', DIRECTORY_SEPARATOR, $className);
    $ds = DIRECTORY_SEPARATOR;
    $fileName = "classes{$ds}{$className}.php";
    if (file_exists($fileName))
    {
        require_once $fileName;
        return true;
    }
    return;
}

spl_autoload_register('planer_autoloader');
