Crafty.c("Grid", {
  _aX: 0,
  _aY: 0,
  _aWidth: 800,
  _aHeight: 600,
  _colorN: "rgb(0,0,0)",
  _widthN: 0.1,

  ready: true,

  init: function() {
      this.requires("Canvas, 2D, Color, Mouse");

      this.bind("Draw", function(obj) {
          this._draw(obj.ctx, obj.pos);
      });
  },

  Grid: function(ax, ay, awidth, aheight, colorn, widthn) {
      this._aX = ax;
      this._aY = ay;
      this._aWidth = awidth;
      this._aHeight = aheight;
      this._colorN = colorn;
      this._widthN = widthn;

      this.attr({
          x: ax,
          y: ay,
          w: awidth,  //Need to add 1 or veritical lines won't draw
          h: aheight   //Need to add 1 or horizontal lines won't draw
      });

      return this;
  },

  _draw: function(ctx, pos) {
    for (var i = 0; i < this._aWidth; i += 10) {
      context.beginPath();
      ctx.strokeStyle = this._colorN || "rgb(0,0,0)";
      ctx.lineWidth = this._widthN || 0.2;
      ctx.moveTo(i, this._aX);
      ctx.lineTo(i, this._aHeight);

      ctx.stroke();
    }

    for (var i = 0; i < this._aHeight; i += 10) {
      context.beginPath();
      ctx.strokeStyle = this._colorN || "rgb(0,0,0)";
      ctx.lineWidth = this._widthN || 0.2;
      ctx.moveTo(this._aX, i);
      ctx.lineTo(this._aWidth, i);

      ctx.stroke();
    }

  }
});
