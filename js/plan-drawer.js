function Editor(elem, options)
{
  /* options */
  this._options = options;
  this._elem = elem;

  /* public value */
  this.mode_create = true;
  this.mode_edit = !this.mode_create;

  this._circlePoint = null;

  this._grid = null;

  this._line_help = null;
  this._line_help_text = null;

  this.points = [];

  /* on events */
  this.onSwitchMode = this._options.onSwitchMode;

  /* log */
  this.log = function (text) {
    if (this._options.debug) console.log(text);
  }

  /* info */
  this.info = function (text) {
    if (this._options.debug) console.info(text);
  }

  this.lockScroll = function (is_lock) {
    if (is_lock) {
      document.getElementsByTagName("body").className = 'stop-scrolling';
    } else {
      document.getElementsByTagName("body").className = '';
    }
  }

  this.formatLineLength = function (line_length) {
    line_length = (line_length/100).toFixed(1);
    line_lengthStr = line_length;
    if (line_length < 1) {
      line_lengthStr = line_length*100 + " см";
    }
    if (line_length >= 1) {
      line_lengthStr = line_length + " м";
    }
    return line_lengthStr;
  }

  this.calcPolygonArea = function () {
    total = 0;

    for (i = 0, l = this.points.length; i < l; i++) {
      addX = this.points[i].x;
      addY = this.points[i == this.points.length - 1 ? 0 : i + 1].y;
      subX = this.points[i == this.points.length - 1 ? 0 : i + 1].x;
      subY = this.points[i].y;

      total += (addX * addY * 0.5);
      total -= (subX * subY * 0.5);
    }

    return Math.abs(total);
  }

  this.switchMode = function (mode) {
    this.info ('Switch mode: ' + mode);
    this.onSwitchMode(mode);
    if (mode == 'create') {
      this.mode_create = true;
      this.mode_edit = !this.mode_create;
      return mode;
    }
    if (mode == 'edit') {
      this.mode_create = false;
      this.mode_edit = !this.mode_create;
      return mode;
    }
    throw "Incorrect mode";
  };

  this.toGrid = function (x)
  {
     nearestGridLine = Math.round ( x/this._options.grid.step ) * this._options.grid.step;
     return Math.abs ( x-nearestGridLine ) < this._options.grid.eps ? nearestGridLine : x;
  }

  this.rightAngle = function () {

  }

  this.drawHelpLine = function (x, y) {
    if (this.points.length >= 1) {
      // создать хелпер если нет
      if (this._line_help != null) {
        this._line_help.destroy();
      }
      this._line_help = Crafty.e("Line").Line(this.points[this.points.length-1].x, this.points[this.points.length-1].y, x, y, 'black', 1);

      if (this._line_help_text != null) {
        this._line_help_text.destroy();
      }

      _line_length = Math.sqrt ( Math.pow(this.points[this.points.length-1].x - x, 2) + Math.pow(this.points[this.points.length-1].y - y, 2) );
      _line_length = this.formatLineLength(_line_length);

      this._line_help_text = Crafty.e("2D, DOM, Text").attr({ x: x, y: y-36 }).text(_line_length).unselectable();
    }
  }

  this.isFirstPoint = function()
  {
    return (this.points.length == 1);
  }

  _this = this;

  this.log('Start app. CraftyJS version: ' + Crafty.getVersion());

  //this.lockScroll(true);
  this.switchMode('create');

  Crafty.init(this._options.size.width, this._options.size.height, document.getElementById(this._elem));

  this._circlePoint = Crafty.e("Circle").Circle(3, "#FF0000");
  this._grid = Crafty.e("Grid").Grid(0, 0, this._options.size.width, this._options.size.height, "gray", 0.2);

  /** zoom function **/
  /*Crafty.bind("MouseWheelScroll", function(evt) {
    Crafty.viewport.scale(Crafty.viewport._scale * (1 + evt.direction * 0.1));
  });*/

  /** grid Mouse Move **/
  this._grid.bind('MouseMove', function(e) {
    if (_this.mode_create) {
      _this._circlePoint.x = _this.toGrid(e.realX)-3;
      _this._circlePoint.y = _this.toGrid(e.realY)-3;
      _this.drawHelpLine(_this.toGrid(e.realX), _this.toGrid(e.realY));
    }
  });

  /** grid Mouse Click **/
  this._grid.bind('Click', function(e) {

    if (_this.mode_create) {
      _this.points[_this.points.length] = {x: _this._circlePoint.x+3, y: _this._circlePoint.y+3};
    }

    if ((_this.points.length >= 3) && _this.mode_create && ( (_this.points[0].x == _this.toGrid(e.realX)) && (_this.points[0].y == _this.toGrid(e.realY)) ) ) {
      _this.switchMode('edit');
      _this._circlePoint.destroy();
      _this._line_help_text.destroy();
    }

    if (!_this.isFirstPoint()) {
      Crafty.e("Line").Line(_this.points[_this.points.length-2].x, _this.points[_this.points.length-2].y, _this.points[_this.points.length-1].x, _this.points[_this.points.length-1].y, "black", 3);
    }
 });

}
