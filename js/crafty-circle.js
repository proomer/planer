Crafty.c("Circle", {

  init: function() {
      this.requires("Canvas, 2D, Color");

      this.bind("Draw", function(obj) {
          this._draw(obj.ctx, obj.pos);
      });
  },


    Circle: function(radius, color) {
        this.radius = radius;
        this.w = this.h = radius * 2;
        this.color = color || "#000000";

        return this;
    },

    _draw: function(ctx, pos) {

       context.beginPath();
       ctx.fillStyle = this.color;
       ctx.arc(
           pos._x + this.radius,
           pos._y + this.radius,
           this.radius,
           0,
           Math.PI * 2
       );
       ctx.closePath();
       ctx.fill();
    }
});
